use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Deserialize, Serialize)]
pub struct DocumentCreateReq {
    pub url: Url,
}
