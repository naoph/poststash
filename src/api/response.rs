use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "result")]
#[serde(rename_all(serialize = "lowercase", deserialize = "lowercase"))]
pub enum DocumentCreateResp {
    /// Provided URL is not a URL
    InvalidUrl,

    /// No extractor could handle this URL
    UnhandledUrl,

    /// This URL has been started already
    Duplicate,

    /// Extraction has been started
    Started { extractor: String},
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "result")]
#[serde(rename_all(serialize = "lowercase", deserialize = "lowercase"))]
pub enum DocumentCreateStatusResp {
    /// Extraction is ongoing
    Ongoing,

    /// Extraction failed
    Failure { message: String },

    /// Extraction succeeded
    Success { document_id: i64 },
}


#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(tag = "result")]
#[serde(rename_all(serialize = "lowercase", deserialize = "lowercase"))]
pub enum DocumentCreateStatusRespWrap {
    /// Provided encoded URL could not be decoded
    InvalidEncodedUrl,

    /// Provided URL is not a URL
    InvalidUrl,

    /// Provided URL has not been seen before
    UnknownUrl,

    /// URL status exists
    Exists { status: DocumentCreateStatusResp },
}
